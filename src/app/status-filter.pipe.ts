import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'statusFilter'
})
export class StatusFilterPipe implements PipeTransform {

  transform(statuses, dateEnd: string): any {
    // хранит текущую дату
    const currentDate = moment([moment().year(), moment().month(), moment().day()]);

    // хранит разницу дней между dateEnd и текущей датой
    // если число >0, то срок эксплуатации истек
    const dateStat = currentDate.diff(moment(dateEnd, 'DD.MM.YYYY'), 'days');

    // если срок эксплуатации истек, вернуть все статусы кроме "В эксплуатации"
    if (dateStat > 0) {
      return statuses.filter(status => status.Id !== 1);
    } else if (dateStat <= 0) {
      return statuses;
    }
    // в любой другой ситуации вернуть просто все статусы
    return statuses;

  }

}
