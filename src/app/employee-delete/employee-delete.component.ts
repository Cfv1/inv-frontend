import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {EmployeesService} from '../employees.service';

@Component({
  selector: 'app-employee-delete',
  templateUrl: './employee-delete.component.html',
  styleUrls: ['./employee-delete.component.css']
})
export class EmployeeDeleteComponent implements OnInit {

  deleted: boolean;
  id: number;
  name: string;

  constructor(private route: ActivatedRoute, private service: EmployeesService) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params: Params) => {
      this.id = params['id'];
      this.name = params['name'];
    });
  }

  deleteEmployee (id: number) {
    this.id = id;
    this.service.destroyEmployee(this.id)
      .subscribe((data) => {
        console.log(data);
      });
    this.deleted = true;
  }

}
