import {Injectable} from '@angular/core';
import {Computer} from './computer';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ComputerService {
  constructor (private http: Http) {}
  computers: Computer[];

  getAllComputers () {
    return this.http.get('http://localhost:55718/getAllComputers')
      .map((responseFromDb: Response) => responseFromDb.json());
  }

  getStatusesForComputers () {
    return this.http.get('http://localhost:55718/getStatus')
      .map((response: Response) => response.json());
  }

  editComputer (computer: any) {
    const data = {
      id: computer.id,
      inventoryNum: computer.inventoryNum,
      monitorManufacturer: computer.monitorManufacturer,
      diagonalSize: computer.diagonalSize,
      serialNum: computer.serialNum,
      amortization: computer.amortization,
      weight: computer.weight,
      price: computer.price,
      dateBegin: computer.dateBegin,
      dateEnd: computer.dateEnd,
      manufacturer: computer.manufacturer,
      modelName: computer.modelName,
      cpu: computer.cpu,
      cpuMemory: computer.cpuMemory,
      coreNum: computer.coreNum,
      ram: computer.ram,
      ramType: computer.ramType,
      hdd: computer.hdd,
      video: computer.video,
      videoNum: computer.videoNum,
      os: computer.os,
      statusid: computer.status,
      employeeid: computer.employee,
      classroomid: computer.classroom
    };
    return this.http.post(`http://localhost:55718/editComputer`, data)
      .map((responseFromDb: Response) => responseFromDb.json());
  }

  addPC (computer: any) {
    const data = {
      inventoryNum: computer.inventoryNum,
      monitorManufacturer: computer.monitorManufacturer,
      diagonalSize: computer.diagonalSize,
      serialNum: computer.serialNum,
      amortization: computer.amortization,
      weight: computer.weight,
      price: computer.price,
      dateBegin: computer.dateBegin,
      dateEnd: computer.dateEnd,
      manufacturer: computer.manufacturer,
      modelName: computer.modelName,
      cpu: computer.cpu,
      cpuMemory: computer.cpuMemory,
      coreNum: computer.coreNum,
      ram: computer.ram,
      ramType: computer.ramType,
      hdd: computer.hdd,
      video: computer.video,
      videoNum: computer.videoNum,
      os: computer.os,
      statusid: computer.status,
      employeeid: computer.employee,
      classroomid: computer.classroom
    };
    return this.http.post(`http://localhost:55718/createComputer`, data)
      .map((responseFromDb: Response) => responseFromDb.json());
  }

  destroyPC (id: number) {
    return this.http.delete(`http://localhost:55718/deleteComputer?id=${id}`);
  }
}
