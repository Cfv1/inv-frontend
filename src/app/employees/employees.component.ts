import {Component, OnInit} from '@angular/core';
import {EmployeesService} from '../employees.service';

interface Employees {
  Id: number;
  Name: string;
  Faculty: string;
  Function: string;
}

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employee: Employees[] = [];
  constructor(private service: EmployeesService) { }

  ngOnInit () {
    this.service.getAllEmployees().subscribe((responseFromService: Employees[]) => {
      this.employee = responseFromService;
    });
  }
}

