import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EmployeesComponent } from './employees/employees.component';
import {EmployeesService} from './employees.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import { NavbarComponent } from './navbar/navbar.component';
import {AppRoutingModule} from './app-routing.module';
import { RoomsComponent } from './rooms/rooms.component';
import { EmployeeDeleteComponent } from './employee-delete/employee-delete.component';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import {RoomService} from './room.service';
import {Room} from './room';
import { RoomEditComponent } from './room-edit/room-edit.component';
import {Block} from './block';
import { RoomDeleteComponent } from './room-delete/room-delete.component';
import { RoomAddComponent } from './room-add/room-add.component';
import { EmployeeAddComponent } from './employee-add/employee-add.component';
import { ComputerComponent } from './computer/computer.component';
import {Computer} from './computer';
import {ComputerService} from './computer.service';
import { ComputerEditComponent } from './computer-edit/computer-edit.component';
import { ComputerAddComponent } from './computer-add/computer-add.component';
import { ComputerDeleteComponent } from './computer-delete/computer-delete.component';
import {Employee} from './employee';
import {Status} from './status';
import { HomeComponent } from './home/home.component';
import { ComputerFilterPipe } from './computer/computer-filter.pipe';
import { StatusFilterPipe } from './status-filter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    EmployeesComponent,
    NavbarComponent,
    RoomsComponent,
    EmployeeAddComponent,
    EmployeeDeleteComponent,
    EmployeeEditComponent,
    RoomEditComponent,
    RoomDeleteComponent,
    RoomAddComponent,
    ComputerComponent,
    ComputerEditComponent,
    ComputerAddComponent,
    ComputerDeleteComponent,
    HomeComponent,
    ComputerFilterPipe,
    StatusFilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [
    EmployeesService,
    RoomService,
    ComputerService,
    Employee, Status, Room, Block, Computer
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
