export class Computer {
  Id: number;
  InventoryNum: string;
  MonitorManufacturer: string;
  DiagonalSize: string;
  SerialNum: string;
  Amortization: number;
  Weight: number;
  Price: number;
  DateBegin: string;
  DateEnd: string;
  Manufacturer: string;
  ModelName: string;
  CPU: string;
  CPUMemory: number;
  CoreNum: number;
  RAM: number;
  RAMType: string;
  HDD: number;
  Video: string;
  VideoNum: number;
  OS: string;
  status: string;
  employee: string;
  classroom: string;
}
