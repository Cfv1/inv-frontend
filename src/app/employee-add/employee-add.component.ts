import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {EmployeesService} from '../employees.service';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.css']
})
export class EmployeeAddComponent implements OnInit {

  added: boolean;

  id: number;
  name: string;
  faculty: string;
  function: string;

  constructor(private route: ActivatedRoute, private service: EmployeesService) { }

  form: FormGroup;

  ngOnInit() {
    this.form = new FormGroup({
      id: new FormControl(),
      name: new FormControl(),
      faculty: new FormControl(),
      function: new FormControl()
    });
  }

  addEmployee () {
    this.service.createEmployee(this.form.value)
      .subscribe((data) => {
        console.log(data);
      });
    this.added = true;
  }

}
