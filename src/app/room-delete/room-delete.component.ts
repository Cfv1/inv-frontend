import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {EmployeesService} from '../employees.service';
import {RoomService} from '../room.service';

@Component({
  selector: 'app-room-delete',
  templateUrl: './room-delete.component.html',
  styleUrls: ['./room-delete.component.css']
})
export class RoomDeleteComponent implements OnInit {

  deleted: boolean;
  id: number;
  name: string;

  constructor(private route: ActivatedRoute, private service: RoomService) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params: Params) => {
      this.id = params['id'];
      this.name = params['name'];
    });
  }

  deleteClassroom (id: number) {
    this.id = id;
    this.service.destroyRoom(this.id)
      .subscribe((data) => {
        console.log(data);
      });
    this.deleted = true;
  }

}
