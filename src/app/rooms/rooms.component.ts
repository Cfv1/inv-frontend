import { Component, OnInit } from '@angular/core';
import {RoomService} from '../room.service';
import {Room} from '../room';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {

  rooms: Room[] = [];
  constructor(private service: RoomService) { }

  ngOnInit() {
    this.getRooms();
    console.log(this.rooms);
  }

  getRooms () {
    this.service.getAllRooms()
      .subscribe((responseFromService: Room[]) => {
        this.rooms = responseFromService;
      });
  }

}
