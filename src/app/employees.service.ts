import {Http} from '@angular/http';
import {Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';

@Injectable()
export class EmployeesService {
  constructor(private http: Http) { }

  createEmployee (employee: any) {
    const data = {name: employee.name, faculty: employee.faculty, function: employee.function};
    return this.http.post(`http://localhost:55718/createEmployee`, data)
      .map((responseFromDb: Response) => responseFromDb.json());
  }

  // Получение списка сотрудников из базы данных
  getAllEmployees () {
    return this.http.get('http://localhost:55718/getAllEmployees')
      .map((responseFromDb: Response) => responseFromDb.json());
  }

  editEmployee (employee: any) {
    const data = {id: employee.id, name: employee.name, faculty: employee.faculty, function: employee.function};
     return this.http.post(`http://localhost:55718/editEmployee`, data)
      .map((responseFromDb: Response) => responseFromDb.json());
  }

  destroyEmployee (id: number) {
    return this.http.delete(`http://localhost:55718/deleteEmployee?id=${id}`);
  }
}
