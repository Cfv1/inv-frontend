import {Component, OnInit} from '@angular/core';
import {RoomService} from '../room.service';
import {Employee} from '../employee';
import {Room} from '../room';
import {Status} from '../status';
import {ComputerService} from '../computer.service';
import {EmployeesService} from '../employees.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params} from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-computer-add',
  templateUrl: './computer-add.component.html',
  styleUrls: ['./computer-add.component.css']
})
export class ComputerAddComponent implements OnInit {
  added: boolean;

  inventoryNum: string;
  monitorManufacturer: string;
  diagonalSize: string;
  serialNum: string;
  amortization: number;
  weight: number;
  price: number;
  dateBegin: string;
  dateEnd: string;
  manufacturer: string;
  modelName: string;
  cpu: string;
  cpuMemory: number;
  coreNum: number;
  ram: number;
  ramType: string;
  hdd: number;
  video: string;
  videoNum: number;
  os: string;
  status: string;
  employee: string;
  classroom: string;

  statuses: Status[] = [];
  employees: Employee[] = [];
  classrooms: Room[] = [];

  constructor(private route: ActivatedRoute,
              private computerService: ComputerService,
              private employeeService: EmployeesService,
              private classroomService: RoomService) { }

  form: FormGroup;

  ngOnInit() {
    this.employeeService.getAllEmployees()
      .subscribe((response: Employee[]) => { this.employees = response; });
    this.classroomService.getAllRooms()
      .subscribe((response: Room[]) => { this.classrooms = response; });
    this.computerService.getStatusesForComputers()
      .subscribe((response: Status[]) => { this.statuses = response; });

    this.route.queryParams.subscribe((params: Params) => {
    //  this.id = params['id'];
      this.inventoryNum = params['inventoryNum'];
      this.monitorManufacturer = params['monitorManufacturer'];
      this.diagonalSize = params['diagonalSize'];
      this.serialNum = params['serialNum'];
      this.amortization = params['amortization'];
      this.weight = params['weight'];
      this.price = params['price'];
      this.dateBegin = params['dateBegin'];
      this.dateEnd = params['dateEnd'];
      this.manufacturer = params['manufacturer'];
      this.modelName = params['modelName'];
      this.cpu = params['cpu'];
      this.cpuMemory = params['cpuMemory'];
      this.coreNum = params['coreNum'];
      this.ram = params['amortization'];
      this.ramType = params['ramType'];
      this.hdd = params['hdd'];
      this.video = params['video'];
      this.videoNum = params['videoNum'];
      this.os = params['os'];
      this.status = params['statusName'];
      this.employee = params['employeeName'];
      this.classroom = params['classroomName'];
    });

    this.form = new FormGroup({
      inventoryNum: new FormControl(),
      monitorManufacturer: new FormControl(),
      diagonalSize: new FormControl(),
      serialNum: new FormControl(),
      amortization: new FormControl(),
      weight: new FormControl(),
      price: new FormControl(),
      dateBegin: new FormControl('', this.checkForDate),
      dateEnd: new FormControl('', this.checkForDate),
      manufacturer: new FormControl(),
      modelName: new FormControl(),
      cpu: new FormControl(),
      cpuMemory: new FormControl(),
      coreNum: new FormControl(),
      ram: new FormControl(),
      ramType: new FormControl(),
      hdd: new FormControl(),
      video: new FormControl(),
      videoNum: new FormControl(),
      os: new FormControl(),
      status: new FormControl('', Validators.required),
      employee: new FormControl('', Validators.required),
      classroom: new FormControl('', Validators.required),
    });
  }


  addComputer () {
      this.computerService.addPC(this.form.value)
        .subscribe((data) => {
          console.log(data);
        });
      this.added = true;
    }

  checkForDate (control: FormControl) {
    if (moment(control.value, 'DD.MM.YYYY').format('DD.MM.YYYY') !== control.value) {
      return {
        'dateFormatError': true
      };
    }
      return null;
  }

  checkAmort () {
    let currentDateBegin = '';
    if (this.form.value.dateBegin === '') {
      currentDateBegin = moment().format('DD.MM.YYYY');
    } else {
      currentDateBegin = moment(this.form.value.dateBegin, 'DD.MM.YYYY').format('DD.MM.YYYY');
    }

    if (this.form.value.dateBegin === '' || this.form.value.amortization === null) {
      console.log('параметры пока пусты');
    } else
    if (parseInt(moment().subtract(moment(currentDateBegin, 'DD.MM.YYYY').year(), 'y').format('YY'), 10) >
      this.form.value.amortization) {
      alert('Срок амортизации истек');
    } else {
      alert('Срок амортизации ещё не истек');
    }
  }


}
