import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {ComputerService} from '../computer.service';

@Component({
  selector: 'app-computer-delete',
  templateUrl: './computer-delete.component.html',
  styleUrls: ['./computer-delete.component.css']
})
export class ComputerDeleteComponent implements OnInit {

  deleted: boolean ;
  id: number;
  name: string;

  constructor(private route: ActivatedRoute, private service: ComputerService) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params: Params) => {
      this.id = params['id'];
      this.name = params['inventoryNum'];
    });
  }

  deleteClassroom (id: number) {
    this.id = id;
    this.service.destroyPC(this.id)
      .subscribe((data) => {
        console.log(data);
      });
    this.deleted = true;
  }


}
