import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {RoomService} from '../room.service';
import {Block} from '../block';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-room-add',
  templateUrl: './room-add.component.html',
  styleUrls: ['./room-add.component.css']
})
export class RoomAddComponent implements OnInit {
  added: boolean;
  id: number;
  name: string;
  type: string;
  block:  string;

  blocks: Block[] = [];

  constructor(private route: ActivatedRoute, private service: RoomService) { }

  form: FormGroup;

  ngOnInit() {
    this.service.getBlocksForRooms()
      .subscribe((response: Block[]) => { this.blocks = response; });


    this.form = new FormGroup({
      id: new FormControl(),
      name: new FormControl(),
      type: new FormControl(),
      block: new FormControl()
    });
  }

  addRoom () {
    this.service.addClassroom(this.form.value)
      .subscribe((data) => {
        console.log(data);
      });
    this.added = true;
  }

}
