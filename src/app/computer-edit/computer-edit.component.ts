import { Component, OnInit } from '@angular/core';
import {Status} from '../status';
import {ActivatedRoute, Params} from '@angular/router';
import {ComputerService} from '../computer.service';
import {Room} from '../room';
import {Employee} from '../employee';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EmployeesService} from '../employees.service';
import {RoomService} from '../room.service';
import * as moment from 'moment';

@Component({
  selector: 'app-computer-edit',
  templateUrl: './computer-edit.component.html',
  styleUrls: ['./computer-edit.component.css']
})
export class ComputerEditComponent implements OnInit {
  edited: boolean;
  id: number;
  inventoryNum: string;
  monitorManufacturer: string;
  diagonalSize: string;
  serialNum: string;
  amortization: number;
  weight: number;
  price: number;
  dateBegin: string;
  dateEnd: string;
  manufacturer: string;
  modelName: string;
  cpu: string;
  cpuMemory: number;
  coreNum: number;
  ram: number;
  ramType: string;
  hdd: number;
  video: string;
  videoNum: number;
  os: string;
  status: string;
  employee: string;
  classroom: string;

  statuses: Status[] = [];
  employees: Employee[] = [];
  classrooms: Room[] = [];

  constructor(private route: ActivatedRoute,
              private computerService: ComputerService,
              private employeeService: EmployeesService,
              private classroomService: RoomService) { }

  form: FormGroup;

  ngOnInit() {
    this.employeeService.getAllEmployees()
      .subscribe((response: Employee[]) => { this.employees = response; });
    this.classroomService.getAllRooms()
      .subscribe((response: Room[]) => { this.classrooms = response; });
    this.computerService.getStatusesForComputers()
      .subscribe((response: Status[]) => { this.statuses = response; });

    this.route.queryParams.subscribe((params: Params) => {
      this.id = params['id'];
      this.inventoryNum = params['inventoryNum'];
      this.monitorManufacturer = params['monitorManufacturer'];
      this.diagonalSize = params['diagonalSize'];
      this.serialNum = params['serialNum'];
      this.amortization = params['amortization'];
      this.weight = params['weight'];
      this.price = params['price'];
      this.dateBegin = params['dateBegin'];
      this.dateEnd = params['dateEnd'];
      this.manufacturer = params['manufacturer'];
      this.modelName = params['modelName'];
      this.cpu = params['cpu'];
      this.cpuMemory = params['cpuMemory'];
      this.coreNum = params['coreNum'];
      this.ram = params['amortization'];
      this.ramType = params['ramType'];
      this.hdd = params['hdd'];
      this.video = params['video'];
      this.videoNum = params['videoNum'];
      this.os = params['os'];
      this.status = params['statusName'];
      this.employee = params['employeeName'];
      this.classroom = params['classroomName'];


    });

    this.form = new FormGroup({
      id: new FormControl(this.id),
      inventoryNum: new FormControl(this.inventoryNum),
      monitorManufacturer: new FormControl(this.monitorManufacturer),
      diagonalSize: new FormControl(this.diagonalSize),
      serialNum: new FormControl(this.serialNum),
      amortization: new FormControl(this.amortization),
      weight: new FormControl(this.weight),
      price: new FormControl(this.price),
      dateBegin: new FormControl(this.dateBegin),
      dateEnd: new FormControl(this.dateEnd),
      manufacturer: new FormControl(this.manufacturer),
      modelName: new FormControl(this.modelName),
      cpu: new FormControl(this.cpu),
      cpuMemory: new FormControl(this.cpuMemory),
      coreNum: new FormControl(this.coreNum),
      ram: new FormControl(this.ram),
      ramType: new FormControl(this.ramType),
      hdd: new FormControl(this.hdd),
      video: new FormControl(this.video),
      videoNum: new FormControl(this.videoNum),
      os: new FormControl(this.os),
      status: new FormControl('', Validators.required),
      employee: new FormControl('', Validators.required),
      classroom: new FormControl('', Validators.required),
    });
  }

  changeComputer() {
    this.computerService.editComputer(this.form.value)
      .subscribe((data) => {
        console.log(data);
      });
    this.edited = true;
  }

  checkAmort () {
    let currentDateBegin = '';
    if (this.form.value.dateBegin === '') {
      currentDateBegin = moment().format('DD.MM.YYYY');
    } else {
      currentDateBegin = moment(this.form.value.dateBegin, 'DD.MM.YYYY').format('DD.MM.YYYY');
    }

    if (this.form.value.dateBegin === '' || this.form.value.amortization === null) {
      console.log('параметры пока пусты');
    } else
    if (parseInt(moment().subtract(moment(currentDateBegin, 'DD.MM.YYYY').year(), 'y').format('YY'), 10) >
      this.form.value.amortization) {
      alert('Срок амортизации истек');
    } else {
      alert('Срок амортизации ещё не истек');
    }
  }
}
