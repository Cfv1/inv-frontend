import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';
import {EmployeesService} from '../employees.service';

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent implements OnInit {
  edited: boolean;
  id: number;
  name: string;
  faculty: string;
  function: string;

  constructor(private route: ActivatedRoute, private service: EmployeesService) { }

  form: FormGroup;

  ngOnInit() {
    this.route.queryParams.subscribe((params: Params) => {
      this.id = params['id'];
      this.name = params['name'];
      this.faculty = params['faculty'];
      this.function = params['function'];
    });

    this.form = new FormGroup({
      id: new FormControl(this.id),
      name: new FormControl(this.name),
      faculty: new FormControl(this.faculty),
      function: new FormControl(this.function)
    });
  }

  changeEmployee() {
     // console.log(this.form.value);
    this.service.editEmployee(this.form.value)
      .subscribe((data) => {
        console.log(data);
      });
    this.edited = true;

  }
}
