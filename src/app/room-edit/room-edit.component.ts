import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';
import {RoomService} from '../room.service';
import {Block} from '../block';
import {Response} from '@angular/http';

@Component({
  selector: 'app-room-edit',
  templateUrl: './room-edit.component.html',
  styleUrls: ['./room-edit.component.css']
})
export class RoomEditComponent implements OnInit {
  edited: boolean;
  id: number;
  name: string;
  type: string;
  block:  string;

  blocks: Block[] = [];

  constructor(private route: ActivatedRoute, private service: RoomService) { }

  form: FormGroup;

  ngOnInit() {
    this.service.getBlocksForRooms()
      .subscribe((response: Block[]) => { this.blocks = response; });

    this.route.queryParams.subscribe((params: Params) => {
      this.id = params['id'];
      this.name = params['name'];
      this.type = params['type'];
      this.block = params['blockName'];
    });

    this.form = new FormGroup({
      id: new FormControl(this.id),
      name: new FormControl(this.name),
      type: new FormControl(this.type),
      block: new FormControl(this.block)
    });
  }

  changeRoom() {
    this.service.editRoom(this.form.value)
      .subscribe((data) => {
        console.log(data);
      });
    this.edited = true;
  }


}

