import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {EmployeesComponent} from './employees/employees.component';
import {EmployeeAddComponent} from './employee-add/employee-add.component';
import {EmployeeEditComponent} from './employee-edit/employee-edit.component';
import {EmployeeDeleteComponent} from './employee-delete/employee-delete.component';

import {RoomsComponent} from './rooms/rooms.component';
import {RoomEditComponent} from './room-edit/room-edit.component';
import {RoomDeleteComponent} from './room-delete/room-delete.component';
import {RoomAddComponent} from './room-add/room-add.component';

import {ComputerComponent} from './computer/computer.component';
import {ComputerAddComponent} from './computer-add/computer-add.component';
import {ComputerEditComponent} from './computer-edit/computer-edit.component';
import {ComputerDeleteComponent} from './computer-delete/computer-delete.component';
import {HomeComponent} from './home/home.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},

  {path: 'employees', component: EmployeesComponent},
  {path: 'addEmployee', component: EmployeeAddComponent},
  {path: 'editEmployee', component: EmployeeEditComponent},
  {path: 'deleteEmployee', component: EmployeeDeleteComponent},

  {path: 'rooms', component: RoomsComponent},
  {path: 'addRoom', component: RoomAddComponent},
  {path: 'editRoom', component: RoomEditComponent},
  {path: 'deleteRoom', component: RoomDeleteComponent},

  {path: 'computers', component: ComputerComponent},
  {path: 'addComputer', component: ComputerAddComponent},
  {path: 'editComputer', component: ComputerEditComponent},
  {path: 'deleteComputer', component: ComputerDeleteComponent},

];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}
