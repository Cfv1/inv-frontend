import {Injectable} from '@angular/core';
import {Room} from './room';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class RoomService {
  constructor (private http: Http) {}
  rooms: Room[];

  addClassroom (room: any) {
    const data = {id: room.id, name: room.name, type: room.type, blockid: room.block};
    return this.http.post(`http://localhost:55718/createClassroom`, data)
      .map((responseFromDb: Response) => responseFromDb.json());
  }

  getAllRooms () {
    return this.http.get('http://localhost:55718/getAllClassrooms')
      .map((responseFromDb: Response) => responseFromDb.json());
  }

  getBlocksForRooms () {
    return this.http.get('http://localhost:55718/getBlock')
      .map((response: Response) => response.json());
  }

  editRoom (room: any) {
    const data = {id: room.id, name: room.name, type: room.type, blockid: room.block};
    return this.http.post(`http://localhost:55718/editClassroom`, data)
      .map((responseFromDb: Response) => responseFromDb.json());
  }

  destroyRoom (id: number) {
    return this.http.delete(`http://localhost:55718/deleteClassroom?id=${id}`);
  }
}
