import {Component, DoCheck, OnInit} from '@angular/core';
import {ComputerService} from '../computer.service';
import {Computer} from '../computer';

@Component({
  selector: 'app-computer',
  templateUrl: './computer.component.html',
  styleUrls: ['./computer.component.css']
})
export class ComputerComponent implements OnInit, DoCheck {
  checkbox: boolean;
  pipeStatus = false;
  pipeRoom = false;
  pipeEmployee = false;
  pipeForStatus = '';
  fieldName = '';
  computers: Computer[] = [];
  constructor(private service: ComputerService) { }

  ngOnInit() {
    this.getComputers();
  }

  ngDoCheck () {
    if (this.pipeStatus) {
      this.fieldName = 'status';
      this.pipeEmployee = false;
      this.pipeRoom = false;
    } else if (this.pipeRoom) {
      this.fieldName = 'classroom';
      this.pipeStatus = false;
      this.pipeEmployee = false;
    } else if (this.pipeEmployee) {
      this.fieldName = 'employee';
      this.pipeRoom = false;
      this.pipeStatus = false;
    }
    this.pipeStatus = false;
    this.pipeEmployee = false;
    this.pipeRoom = false;
    console.log(this.fieldName);
  }

  getComputers () {
    this.service.getAllComputers()
      .subscribe((responseFromService: Computer[]) => {
        this.computers = responseFromService;
      });
  }

}
