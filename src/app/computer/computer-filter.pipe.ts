import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'computerFilter'
})
export class ComputerFilterPipe implements PipeTransform {

  transform(computers, pipeForStatus: string, fieldName: string): any {
    if (computers.length === 0 || pipeForStatus === '' || fieldName === '') {
      return computers;
    }

    return computers.filter(computer => computer[fieldName].toLowerCase()
      .indexOf(pipeForStatus.toLowerCase()) !== -1);
  }

}
